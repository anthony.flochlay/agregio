package com.agregio.controller.market;

import com.agregio.domain.Market;

public class MarketRepresentation {
    public static String of(Market market) {
        return switch (market) {
            case PRIMARY -> "primary";
            case SECONDARY -> "secondary";
            case FAST -> "fast";
        };
    }
}

package com.agregio.controller.market;

import com.agregio.domain.Market;

import java.util.Arrays;

public class MarketConverter {
    public static Market marketOf(String market) {
        return Arrays.stream(Market.values())
                .filter(f -> MarketRepresentation.of(f).equals(market))
                .findFirst()
                .orElseThrow(() -> new MarketNotFoundException("Unknown market: " + market));
    }
}

package com.agregio.controller.market;

import com.agregio.domain.Market;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/markets")
public class MarketController {
    @GetMapping
    public List<String> markets() {
        return Arrays.stream(Market.values()).map(MarketRepresentation::of).toList();
    }
}

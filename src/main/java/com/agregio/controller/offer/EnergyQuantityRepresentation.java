package com.agregio.controller.offer;

import com.agregio.domain.EnergyQuantity;

public record EnergyQuantityRepresentation(long energy, String unit) {
    public static EnergyQuantityRepresentation of(EnergyQuantity energyQuantity) {
        return new EnergyQuantityRepresentation(energyQuantity.toWatts(), "W");
    }

    public EnergyQuantity toDomain() {
        return switch (unit) {
            case "W" -> EnergyQuantity.ofWatts(energy);
            case "kW" -> EnergyQuantity.ofKiloWatts(energy);
            case "MW" -> EnergyQuantity.ofMegaWatts(energy);
            case "GW" -> EnergyQuantity.ofGigaWatts(energy);
            default -> throw new UnknownEnergyUnitException("Unknown unit: " + unit);
        };
    }

    private static class UnknownEnergyUnitException extends RuntimeException {
        public UnknownEnergyUnitException(String message) {
            super(message);
        }
    }
}

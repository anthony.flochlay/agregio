package com.agregio.controller.offer;

import com.agregio.controller.market.MarketConverter;
import com.agregio.domain.Block;
import com.agregio.domain.Offer;
import com.agregio.domain.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static com.agregio.controller.market.MarketConverter.marketOf;

@RestController
@RequestMapping("/offers")
public class OfferController {

    @Autowired
    private OfferService offerService;

    @GetMapping
    public List<OfferRepresentation> searchOffers(@RequestParam("market") Optional<String> marketCriteria) {
        Predicate<Offer> searchCriteria = offer ->
                marketCriteria
                        .map(MarketConverter::marketOf)
                        .map(market -> offer.market() == market)
                        .orElse(true);
        return OfferRepresentation.of(
                offerService.searchOffers(searchCriteria)
        );
    }

    @GetMapping("/{offerId}")
    public OfferRepresentation getOffer(@PathVariable long offerId) {
        return offerService.getById(offerId)
                .map(OfferRepresentation::new)
                .orElseThrow(OfferNotFoundException::new);
    }

    @PostMapping
    public OfferRepresentation createOffer(@RequestBody OfferRepresentation offer) {
        Offer createdOffer = offerService.create(
                offer.name(),
                marketOf(offer.market()),
                blocksOf(offer.blocks())
        );
        return OfferRepresentation.of(createdOffer);
    }

    private List<Block> blocksOf(List<BlockRepresentation> blocks) {
        return blocks.stream()
                .map(b -> new Block(b.energyQuantity().toDomain(), b.farmId()))
                .toList();
    }

}

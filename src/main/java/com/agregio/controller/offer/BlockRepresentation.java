package com.agregio.controller.offer;

import com.agregio.domain.Block;

import java.util.List;

public record BlockRepresentation(EnergyQuantityRepresentation energyQuantity, long farmId) {
    public BlockRepresentation(Block block) {
        this(EnergyQuantityRepresentation.of(block.energyQuantity()), block.farmId());
    }

    public static List<BlockRepresentation> of(List<Block> blocks) {
        return blocks.stream().map(BlockRepresentation::of).toList();
    }

    public static BlockRepresentation of(Block block) {
        return new BlockRepresentation(block);
    }

    public BlockRepresentation withFarmId(long farmId) {
        return new BlockRepresentation(this.energyQuantity, farmId);
    }
}

package com.agregio.controller.offer;

import com.agregio.controller.market.MarketRepresentation;
import com.agregio.domain.Offer;

import java.util.List;

public record OfferRepresentation(Long id, String name, String market, List<BlockRepresentation> blocks) {
    public OfferRepresentation(String name, String market, List<BlockRepresentation> blocks) {
        this(null, name, market, blocks);
    }

    public OfferRepresentation(Offer offer) {
        this(offer.id(), offer.name(), MarketRepresentation.of(offer.market()), BlockRepresentation.of(offer.blocks()));
    }

    public static OfferRepresentation of(Offer offer) {
        return new OfferRepresentation(offer);
    }

    public static OfferRepresentation of(String name, String market, BlockRepresentation... blocks) {
        return new OfferRepresentation(name, market, List.of(blocks));
    }

    public static List<OfferRepresentation> of(List<Offer> offers) {
        return offers.stream()
                .map(OfferRepresentation::of)
                .toList();
    }

    public OfferRepresentation withMarket(String otherMarket) {
        return new OfferRepresentation(
                this.name,
                otherMarket,
                this.blocks
        );
    }

}

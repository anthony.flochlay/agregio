package com.agregio.controller.farm;

import com.agregio.domain.Farm;

import java.util.List;

public record FarmRepresentation(Long id, String name) {

    public FarmRepresentation(Farm farm) {
        this(farm.id(), farm.name());
    }

    public static FarmRepresentation of(Farm farm) {
        return new FarmRepresentation(farm);
    }

    public static List<FarmRepresentation> of(List<Farm> farms) {
        return farms.stream().map(FarmRepresentation::of).toList();
    }

    public FarmRepresentation withFarmId(Long farmId) {
        return new FarmRepresentation(farmId, this.name);
    }
}

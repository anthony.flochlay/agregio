package com.agregio.controller.farm;

import com.agregio.domain.Farm;
import com.agregio.domain.FarmNotFoundException;
import com.agregio.domain.FarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.agregio.controller.market.MarketConverter.marketOf;

@RestController
@RequestMapping("/farms")
public class FarmController {
    @Autowired
    FarmService farmService;

    @GetMapping
    public List<FarmRepresentation> searchFarms(@RequestParam("market") String market) {
        return FarmRepresentation.of(
                farmService.searchFarmsByMarket(marketOf(market))
        );
    }

    @GetMapping("/{farmId}")
    public FarmRepresentation getFarm(@PathVariable long farmId) {
        return farmService.getFarm(farmId)
                .map(FarmRepresentation::new)
                .orElseThrow(() -> new FarmNotFoundException("Unknown farm: " + farmId));
    }

    @PostMapping
    public FarmRepresentation createFarm(@RequestBody FarmRepresentation farm) {
        Farm createdFarm = farmService.createFarm(farm.name());
        return new FarmRepresentation(createdFarm);
    }
}

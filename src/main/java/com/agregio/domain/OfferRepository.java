package com.agregio.domain;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;

@Repository
public class OfferRepository {

    public static final Random RANDOM = new Random();

    private final List<Offer> offers = new ArrayList<>(); // TODO: use a better implementation when more data

    public List<Offer> findAll() {
        return find(offer -> true);
    }

    public List<Offer> find(Predicate<Offer> predicate) {
        return offers.stream()
                .filter(predicate)
                .toList();
    }

    public Optional<Offer> getById(long offerId) {
        return offers.stream().filter(offer -> offer.id() == offerId).findFirst(); // TODO: use find
    }

    public Offer create(String name, Market market, List<Block> blocks) { // TODO: introduce object parameter
        Offer offer = new Offer(nextId(), name, market, blocks);
        offers.add(offer);
        return offer;
    }

    private long nextId() {
        return RANDOM.nextLong();
    }
}

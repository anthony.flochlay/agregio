package com.agregio.domain;

public enum Market {
    PRIMARY,
    SECONDARY,
    FAST
}

package com.agregio.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static java.text.MessageFormat.format;

@Service
public class OfferService {
    private final OfferRepository offerRepository;
    private final FarmService farmService;

    @Autowired
    public OfferService(OfferRepository offerRepository, FarmService farmService) {
        this.offerRepository = offerRepository;
        this.farmService = farmService;
    }

    public List<Offer> searchOffers(Predicate<Offer> criteria) {
        return offerRepository.find(criteria);
    }

    public List<Offer> searchOffersByMarket(Market market) {
        return searchOffers(offer -> offer.market() == market);
    }

    public Optional<Offer> getById(long offerId) {
        return offerRepository.getById(offerId);
    }

    public Offer create(String name, Market market, List<Block> blocks) {
        checkBlockFarmsExist(blocks);
        return offerRepository.create(name, market, blocks);
    }

    private void checkBlockFarmsExist(List<Block> blocks) {
        for (Block block : blocks) {
            long farmId = block.farmId();
            if (farmService.getFarm(farmId).isEmpty()) {
                throw new FarmNotFoundException(format("Unknown farm id {0} for block: {1}", farmId, block));
            }
        }
    }
}
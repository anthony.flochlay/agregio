package com.agregio.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FarmService {
    @Autowired
    private FarmRepository farmRepository;
    @Lazy //TODO: redesign to prevent circular dependencies
    @Autowired
    private OfferService offerService;

    public List<Farm> searchFarmsByMarket(Market market) {
        List<Offer> offers = offerService.searchOffersByMarket(market);
        return farmRepository.getByIds(
                farmIdsOf(offers)
        );
    }

    private static List<Long> farmIdsOf(List<Offer> offers) {
        return offers.stream().flatMap(o -> o.blocks().stream()).map(Block::farmId).distinct().toList();
    }

    public Optional<Farm> getFarm(long farmId) {
        return farmRepository.getById(farmId);
    }

    public Farm createFarm(String name) {
        return farmRepository.create(name);
    }
}
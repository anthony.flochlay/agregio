package com.agregio.domain;

/**
 * A block of energy
 * @param energyQuantity: the quantity of energy produced in that block
 * @param farmId: the id of the farm producing the energy
 */
//TODO: add period (ex: 06:00AM to 09:00 AM) and minimum price
public record Block(EnergyQuantity energyQuantity, long farmId) {
    public Block withFarmId(long theFarmId) {
        return new Block(this.energyQuantity, theFarmId);
    }
}

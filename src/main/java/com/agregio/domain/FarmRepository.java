package com.agregio.domain;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

// TODO: add unit tests
@Repository
public class FarmRepository {

    public static final Random RANDOM = new Random();

    private final List<Farm> farms = new ArrayList<>(); // TODO: use a better implementation when more data

    public Optional<Farm> getById(long farmId) {
        return farms.stream().filter(farm -> farm.id() == farmId).findFirst();
    }

    public List<Farm> getByIds(List<Long> farmIds) {
        return farms.stream().filter(farm -> farmIds.contains(farm.id())).toList();
    }

    public Farm create(String name) {
        Farm farm = new Farm(nextId(), name);
        farms.add(farm);
        return farm;
    }

    private long nextId() {
        return RANDOM.nextLong();
    }
}

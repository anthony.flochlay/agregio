package com.agregio.domain;

import java.util.List;

/**
 * An energy offer
 * @param id: the id of the offer
 * @param name: the name of the offer
 * @param market: the market on which the offer is proposed
 * @param blocks: the blocks of the offer
 */
public record Offer(long id, String name, Market market, List<Block> blocks) {
}

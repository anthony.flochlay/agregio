package com.agregio.domain;

public record Farm(long id, String name) {
}

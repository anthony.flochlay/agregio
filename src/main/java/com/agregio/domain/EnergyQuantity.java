package com.agregio.domain;

import java.util.Objects;

public class EnergyQuantity {

    private final long energyInWatt;

    private EnergyQuantity(long energyInWatt) {
        this.energyInWatt = energyInWatt;
    }

    public static EnergyQuantity ofWatts(long watts) {
        return new EnergyQuantity(watts);
    }

    public static EnergyQuantity ofKiloWatts(long kiloWatts) {
        return ofWatts(1_000 * kiloWatts);
    }

    public static EnergyQuantity ofMegaWatts(long megaWatts) {
        return ofKiloWatts(1_000 * megaWatts);
    }

    public static EnergyQuantity ofGigaWatts(long gigaWatts) {
        return EnergyQuantity.ofMegaWatts(1_000 * gigaWatts);
    }

    public long toWatts() {
        return energyInWatt;
    }

    @Override
    public String toString() {
        return energyInWatt + "W";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnergyQuantity that = (EnergyQuantity) o;
        return energyInWatt == that.energyInWatt;
    }

    @Override
    public int hashCode() {
        return Objects.hash(energyInWatt);
    }
}

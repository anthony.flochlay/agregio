package com.agregio.controller.offer;

import com.agregio.domain.Offer;
import com.agregio.domain.OfferService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static com.agregio.controller.offer.OfferRepresentationFixtures.anOfferRepresentation;
import static com.agregio.domain.OfferFixtures.anOffer;
import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OfferController.class)
public class OfferControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    OfferService offerService;

    @Test
    void search_offers_return_empty_if_no_offer() throws Exception {
        given(offerService.searchOffers(any())).willReturn(emptyList());

        mockMvc.perform(get("/offers")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    void search_offers_return_the_offers_if_any() throws Exception {
        List<Offer> theOffers = List.of(anOffer(), anOffer());
        given(offerService.searchOffers(any())).willReturn(theOffers);

        mockMvc.perform(get("/offers")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(theOffers.get(0).name())))
                .andExpect(jsonPath("$[0].blocks[0].farmId", is(theOffers.get(0).blocks().get(0).farmId())))
                .andExpect(jsonPath("$[1].name", is(theOffers.get(1).name())))
                .andExpect(jsonPath("$[1].blocks[0].farmId", is(theOffers.get(1).blocks().get(0).farmId())));
    }

    @Test
    void get_offer_return_the_offer() throws Exception {
        var theExistingOffer = anOffer(999);
        given(offerService.getById(theExistingOffer.id())).willReturn(Optional.of(theExistingOffer));

        mockMvc.perform(get("/offers/{offerId}", theExistingOffer.id())
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(theExistingOffer.id()))
                .andExpect(jsonPath("name").value(theExistingOffer.name()))
                .andExpect(jsonPath("blocks[0].farmId", is(theExistingOffer.blocks().get(0).farmId())));
    }

    @Test
    void get_offer_return_404_if_the_offer_does_not_exist() throws Exception {
        given(offerService.getById(anyLong())).willReturn(Optional.empty());

        mockMvc.perform(get("/offers/{offerId}", 999L)
                        .contentType("application/json"))
                .andExpect(status().is(404));
    }

    @Test
    void create_offer_should_create_the_offer() throws Exception {
        Offer theOffer = anOffer(999L);
        given(offerService.create(theOffer.name(), theOffer.market(), theOffer.blocks())).willReturn(theOffer);

        mockMvc.perform(post("/offers")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(new OfferRepresentation(theOffer))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(theOffer.id()))
                .andExpect(jsonPath("name").value(theOffer.name()))
                .andExpect(jsonPath("blocks[0].farmId", is(theOffer.blocks().get(0).farmId())));
    }

    @Test
    void create_offer_should_fail_when_market_does_not_exist() throws Exception {
        var theNewOfferRepresentation = anOfferRepresentation().withMarket("unknownMarket");

        mockMvc.perform(post("/offers")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(theNewOfferRepresentation)))
                .andExpect(status().is(404));
    }
}

package com.agregio.controller.offer;

import static com.agregio.domain.OfferFixtures.anOffer;

public class OfferRepresentationFixtures {

    public static OfferRepresentation anOfferRepresentation() {
        return OfferRepresentation.of(anOffer());
    }

}
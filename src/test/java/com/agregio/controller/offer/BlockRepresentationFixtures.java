package com.agregio.controller.offer;

import com.agregio.domain.BlockFixtures;

public class BlockRepresentationFixtures {

    public static BlockRepresentation aBlockRepresentation() {
        return BlockRepresentation.of(BlockFixtures.aBlock());
    }

    public static BlockRepresentation aBlockRepresentationOf(long farmId) {
        return aBlockRepresentation().withFarmId(farmId);
    }
}

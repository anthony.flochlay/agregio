package com.agregio.controller.farm;

import com.agregio.domain.FarmService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FarmController.class)
public class FarmControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    FarmService farmService;

    // TODO: add integration tests

    // TODO: searchByMarket should return 400 if unknown market

    @Test
    void get_farm_return_404_if_the_farm_does_not_exist() throws Exception {
        given(farmService.getFarm(anyLong())).willReturn(Optional.empty());

        mockMvc.perform(get("/farms/{farmId}", 999L)
                        .contentType("application/json"))
                .andExpect(status().is(404));
    }


}
package com.agregio.controller.farm;

import static com.agregio.domain.FarmFixtures.aFarm;

public class FarmRepresentationFixtures {

    public static FarmRepresentation aFarmRepresentation() {
        return new FarmRepresentation(aFarm());
    }

    public static FarmRepresentation aFarmRepresentation(Long farmId) {
        return aFarmRepresentation().withFarmId(farmId);
    }
}

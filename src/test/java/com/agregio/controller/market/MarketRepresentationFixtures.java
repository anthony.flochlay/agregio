package com.agregio.controller.market;

import com.agregio.domain.Market;
import com.agregio.domain.MarketFixtures;

public class MarketRepresentationFixtures {

    public static String aMarket() {
        return MarketRepresentation.of(MarketFixtures.aMarket());
    }

    public static String aMarketOtherThan(String excluded) {
        return aMarketOtherThan(MarketConverter.marketOf(excluded));
    }

    public static String aMarketOtherThan(Market excluded) {
        return MarketRepresentation.of(
                MarketFixtures.aMarketOtherThan(excluded)
        );
    }

}

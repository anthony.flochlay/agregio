package com.agregio;

import com.agregio.controller.farm.FarmRepresentation;
import com.agregio.controller.farm.FarmRepresentationFixtures;
import com.agregio.controller.offer.OfferRepresentation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.stream.LongStream;

import static com.agregio.controller.offer.BlockRepresentationFixtures.aBlockRepresentationOf;
import static com.agregio.controller.farm.FarmRepresentationFixtures.aFarmRepresentation;
import static com.agregio.controller.market.MarketRepresentationFixtures.aMarket;
import static com.agregio.controller.market.MarketRepresentationFixtures.aMarketOtherThan;
import static java.text.MessageFormat.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class AgregioApplicationTest {

    @Autowired
    TestRestTemplate restTemplate;

    @Test
    void there_are_3_markets() {
        @SuppressWarnings("unchecked")
        List<String> markets = restTemplate.getForObject("/markets", List.class);
        assertThat(markets).containsExactly("primary", "secondary", "fast");
    }

    @Test
    void get_the_offer_returns_the_created_offer_with_an_id() {
        // Given
        String theMarket = aMarket();
        var theFarm = anExistingFarm();
        var theCreatedOffer = createOffer(OfferRepresentation.of("theOffer", theMarket, aBlockRepresentationOf(theFarm.id())));
        // When
        var theReadOffer = restTemplate.getForObject("/offers/" + theCreatedOffer.id(), OfferRepresentation.class);
        // Then
        assertThat(theReadOffer.id()).isEqualTo(theCreatedOffer.id());
        assertThat(theReadOffer.name()).isEqualTo(theCreatedOffer.name());

        var actualBlock = theReadOffer.blocks().get(0);
        assertThat(actualBlock.energyQuantity()).isEqualTo(theCreatedOffer.blocks().get(0).energyQuantity());
        assertThat(actualBlock.farmId()).isEqualTo(theCreatedOffer.blocks().get(0).farmId());
    }

    private OfferRepresentation createOffer(OfferRepresentation newOffer) {
        return restTemplate.postForObject("/offers", newOffer, OfferRepresentation.class);
    }

    @Test
    void list_offers_by_market() {
        // Given
        String theMarket = aMarket();
        var theFarm = anExistingFarm();
        createOffers(
                OfferRepresentation.of("theOffer1", theMarket, aBlockRepresentationOf(theFarm.id())),
                OfferRepresentation.of("theOffer2", theMarket, aBlockRepresentationOf(theFarm.id())),
                OfferRepresentation.of("theOfferOnAnotherMarket", aMarketOtherThan(theMarket), aBlockRepresentationOf(theFarm.id()))
        );
        // When
        var theGotOffers = restTemplate.getForObject(format("/offers?market={0}", theMarket), OfferRepresentation[].class);
        // Then
        assertThat(theGotOffers)
                .extracting(OfferRepresentation::name)
                .contains("theOffer1", "theOffer2")
                .doesNotContain("theOfferOnAnotherMarket");
    }

    private FarmRepresentation anExistingFarm() {
        return createFarm(aFarmRepresentation());
    }

    @Test
    void get_the_farm_returns_the_created_farm_with_an_id() {
        var theFarmToCreate = aFarmRepresentation();
        var theCreatedFarm = createFarm(theFarmToCreate);
        var theGotFarm = restTemplate.getForObject("/farms/" + theCreatedFarm.id(), FarmRepresentation.class);
        assertThat(theGotFarm.id()).isEqualTo(theCreatedFarm.id());
        assertThat(theGotFarm.name()).isEqualTo(theFarmToCreate.name());
    }

    private FarmRepresentation createFarm(FarmRepresentation farmToCreate) {
        return restTemplate.postForObject("/farms", farmToCreate, FarmRepresentation.class);
    }

    @Test
    void list_farms_by_market() {
        // Given
        var theMarket = aMarket();
        var theFarms = someExistingFarms(3);
        createOffers(
                OfferRepresentation.of(
                        "theOffer1OnMarket",
                        theMarket,
                        aBlockRepresentationOf(theFarms.get(0).id()),
                        aBlockRepresentationOf(theFarms.get(1).id())
                ),
                OfferRepresentation.of(
                        "theOffer2OnMarket",
                        theMarket,
                        aBlockRepresentationOf(theFarms.get(0).id())
                ),
                OfferRepresentation.of(
                        "theOfferOnOtherMarket",
                        aMarketOtherThan(theMarket),
                        aBlockRepresentationOf(theFarms.get(2).id())
                )
        );
        // When
        var theFarmsSellingOnMarket = restTemplate.getForObject(format("/farms?market={0}", theMarket), FarmRepresentation[].class);
        // Then
        assertThat(theFarmsSellingOnMarket)
                .extracting(FarmRepresentation::name)
                .contains(theFarms.get(0).name(), theFarms.get(1).name())
                .doesNotContain(theFarms.get(2).name());
    }

    private List<FarmRepresentation> someExistingFarms(int size) {
        return LongStream.range(0, size)
                .mapToObj(FarmRepresentationFixtures::aFarmRepresentation)
                .map(this::createFarm)
                .toList();
    }

    private List<OfferRepresentation> createOffers(OfferRepresentation... offers) {
        return Arrays.stream(offers).map(this::createOffer).toList();
    }

}
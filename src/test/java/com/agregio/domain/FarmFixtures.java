package com.agregio.domain;

import java.util.Random;

public class FarmFixtures {

    public static final Random RANDOM = new Random();

    public static Farm aFarm() {
        return aFarm(nextId());
    }

    private static Farm aFarm(long id) {
        return new Farm(id, "theFarm" + id);
    }

    private static long nextId() {
        return RANDOM.nextLong();
    }
}

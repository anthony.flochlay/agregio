package com.agregio.domain;

import java.util.List;
import java.util.Random;

import static com.agregio.domain.EnergyQuantityFixtures.anEnergyQuantity;

public class BlockFixtures {

    public static final Random RANDOM = new Random();

    public static List<Block> someBlocks() {
        // TODO: make it consistant (ex: 8 blocks of 3 hours for a day)
        return List.of(aBlock(), aBlock());
    }

    public static Block aBlock() {
        return new Block(anEnergyQuantity(), aFarmId());
    }

    private static long aFarmId() {
        return RANDOM.nextLong();
    }
}

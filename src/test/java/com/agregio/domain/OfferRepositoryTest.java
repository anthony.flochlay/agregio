package com.agregio.domain;

import org.junit.jupiter.api.Test;

import java.util.List;

import static com.agregio.domain.BlockFixtures.someBlocks;
import static com.agregio.domain.OfferFixtures.anOffer;
import static org.assertj.core.api.Assertions.assertThat;

class OfferRepositoryTest {

    private final OfferRepository offerRepository = new OfferRepository();

    @Test
    void find_all_return_empty_when_no_offer() {
        // Given no offer
        //When
        List<Offer> offers = offerRepository.findAll();
        // Then
        assertThat(offers).isEmpty();
    }

    @Test
    void find_all_return_the_offers_if_any() {
        // Given
        Offer theOffer1 = anExistingOffer();
        Offer theOffer2 = anExistingOffer();
        //When
        List<Offer> offers = offerRepository.findAll();
        // Then
        assertThat(offers)
                .extracting(Offer::name)
                .containsExactly(theOffer1.name(), theOffer2.name());
    }

    private Offer anExistingOffer() {
        Offer offer = anOffer();
        offerRepository.create(offer.name(), offer.market(), someBlocks());
        return offer;
    }

    @Test
    void get_offer_return_the_offer() {
        Offer theOffer = anOffer();
        var theExistingOffer = offerRepository.create(theOffer.name(), theOffer.market(), theOffer.blocks());
        var actualOffer = offerRepository.getById(theExistingOffer.id());
        assertThat(actualOffer).hasValue(theExistingOffer);
    }

    @Test
    void get_offer_return_empty_when_offer_does_not_exist() {
        var actualOffer = offerRepository.getById(999);
        assertThat(actualOffer).isEmpty();
    }
}
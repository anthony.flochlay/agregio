package com.agregio.domain;

import java.util.Random;

import static com.agregio.domain.BlockFixtures.someBlocks;
import static com.agregio.domain.MarketFixtures.aMarket;

public class OfferFixtures {

    public static final Random RANDOM = new Random();

    public static Offer anOffer() {
        return anOffer(RANDOM.nextLong());
    }

    public static Offer anOffer(long id) {
        return new Offer(id, "theOffer" + id, aMarket(), someBlocks());
    }
}

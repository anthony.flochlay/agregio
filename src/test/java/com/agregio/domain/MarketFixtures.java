package com.agregio.domain;

import java.util.Arrays;

public class MarketFixtures {

    public static Market aMarket() {
        return Arrays.stream(Market.values()).findAny().orElseThrow();
    }

    public static Market aMarketOtherThan(Market excluded) {
        return Arrays.stream(Market.values()).filter(m -> !m.equals(excluded)).findAny().orElseThrow();
    }
}

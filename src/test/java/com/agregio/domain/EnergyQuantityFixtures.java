package com.agregio.domain;

import java.util.Random;

public class EnergyQuantityFixtures {

    public static final Random RANDOM = new Random();

    public static EnergyQuantity anEnergyQuantity() {
        return EnergyQuantity.ofMegaWatts(RANDOM.nextInt(1_000));
    }
}

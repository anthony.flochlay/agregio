package com.agregio.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.agregio.domain.BlockFixtures.aBlock;
import static com.agregio.domain.MarketFixtures.aMarket;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
class OfferServiceTest {

    @Mock
    OfferRepository offerRepository;

    @Mock
    FarmService farmService;

    @InjectMocks
    private OfferService offerService;

    @Test
    void create_offer_should_fail_when_farm_does_not_exist() {
        Block theBlock = aBlock().withFarmId(999L);
        assertThatThrownBy(
                () -> offerService.create("theOffer", aMarket(), List.of(theBlock))
        ).isInstanceOf(FarmNotFoundException.class)
                .hasMessageStartingWith("Unknown farm id 999 for block: Block");
    }
}
package com.agregio.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class EnergyQuantityTest {

    @Test
    void should_create_quantity_in_watt() {
        assertThat(EnergyQuantity.ofWatts(5).toWatts()).isEqualTo(5);
    }

    @Test
    void should_create_quantity_in_kilo_watt() {
        assertThat(EnergyQuantity.ofKiloWatts(5).toWatts()).isEqualTo(5_000);
    }

    @Test
    void should_create_quantity_in_mega_watt() {
        assertThat(EnergyQuantity.ofMegaWatts(5).toWatts()).isEqualTo(5_000_000);
    }

    @Test
    void should_create_quantity_in_giga_watt() {
        assertThat(EnergyQuantity.ofGigaWatts(5).toWatts()).isEqualTo(5_000_000_000L);
    }

    @Test
    void should_equal_itself() {
        EnergyQuantity energy = EnergyQuantity.ofMegaWatts(1);
        assertThat(energy).isEqualTo(energy);
    }

    @Test
    void should_equal_in_another_unit() {
        assertThat(
                EnergyQuantity.ofKiloWatts(1_000)
        ).isEqualTo(
                EnergyQuantity.ofMegaWatts(1)
        );
    }
}